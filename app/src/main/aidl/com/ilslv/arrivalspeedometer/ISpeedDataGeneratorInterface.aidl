package com.ilslv.arrivalspeedometer;

import com.ilslv.arrivalspeedometer.DataChangedListener;

interface ISpeedDataGeneratorInterface {

    oneway void generateValues(in DataChangedListener onDataChangedListener);
}