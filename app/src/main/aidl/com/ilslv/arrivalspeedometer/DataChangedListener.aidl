package com.ilslv.arrivalspeedometer;

interface DataChangedListener {
    void onDataChanged(in float value);
}