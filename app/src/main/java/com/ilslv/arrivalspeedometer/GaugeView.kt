package com.ilslv.arrivalspeedometer

import android.content.Context
import android.graphics.*
import android.graphics.Paint.*
import android.util.AttributeSet
import android.view.View
import kotlin.math.abs
import kotlin.math.hypot
import kotlin.math.sign
import android.text.TextPaint

class GaugeView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyle: Int = 0
) : View(context, attrs, defStyle) {
    
    private var hasSecondaryTextSize = true
    private var scaleColor = Color.BLACK
    
    private var warningZoneColor = Color.RED
    private var isMainTextVisible = true
    private var mainTextUnit = context.getString(R.string.speed_unit)
    
    private val scaleRect = RectF(
        SCALE_POSITION,
        SCALE_POSITION,
        SCALE_POSITION,
        SCALE_POSITION
    )
    
    private var background: Bitmap? = null
    private val backgroundPaint: Paint = Paint().apply {
        isFilterBitmap = true
    }
    
    private val rangePaint = Paint(LINEAR_TEXT_FLAG or ANTI_ALIAS_FLAG).apply {
        style = Style.STROKE
        textSize = 0.05f
        strokeWidth = 0.003f
        typeface = Typeface.SANS_SERIF
        textAlign = Align.CENTER
        color = scaleColor
    }
    
    private val rangePaintSecondary = Paint(LINEAR_TEXT_FLAG or ANTI_ALIAS_FLAG).apply {
        style = Style.STROKE
        textSize = 0.03f
        strokeWidth = 0.003f
        typeface = Typeface.SANS_SERIF
        textAlign = Align.CENTER
        color = scaleColor
    }
    
    private var arrowRightPath = Path().apply {
        moveTo(0.5f, 0.5f)
        lineTo(0.5f + ARROW_WIDTH, 0.5f)
        lineTo(0.5f, 0.5f - ARROW_HEIGHT)
        lineTo(0.5f, 0.5f)
        lineTo(0.5f + ARROW_WIDTH, 0.5f)
    }
    
    private var arrowLeftPath = Path().apply {
        moveTo(0.5f, 0.5f)
        lineTo(0.5f - ARROW_WIDTH, 0.5f)
        lineTo(0.5f, 0.5f - ARROW_HEIGHT)
        lineTo(0.5f, 0.5f)
        lineTo(0.5f - ARROW_WIDTH, 0.5f)
    }
    
    private val arrowPaint = Paint(ANTI_ALIAS_FLAG).apply {
        color = Color.RED
    }
    
    private val arrowCirclePaint = Paint(ANTI_ALIAS_FLAG).apply {
        color = Color.BLACK
    }
    
    private val arrowInnerCirclePaint = Paint(ANTI_ALIAS_FLAG).apply {
        style = Style.STROKE
        color = Color.WHITE
        strokeWidth = 0.005f
    }
    
    private val gaugeRingPaint = Paint(ANTI_ALIAS_FLAG).apply {
        style = Style.STROKE
        color = scaleColor
        strokeWidth = 0.01f
    }
    
    private val textPaint = TextPaint(LINEAR_TEXT_FLAG).apply {
        textSize = 0.08f
        typeface = Typeface.SANS_SERIF
        textAlign = Align.CENTER
        color = Color.BLACK
    }
    
    private var scaleRotation = 0f
    private var divisionValue = 0f
    private var subdivisionValue = 0f
    private var subdivisionAngle = 0f
    private var targetValue = 0f
    private var currentValue = 0f
    private var arrowVelocity = 0f
    private var arrowAcceleration = 0f
    private var arrowLastMoved: Long = -1
    private var arrowInitialized = false
    
    private var scaleStartValue = 0.0f
    private var scaleEndValue = 100.0f
    private var scaleDivisions = 10
    private var scaleSubdivisions = 5
    private var isWarningZoneEnabled = false
    private var warningZoneStartValue = 0.0f
    private var warningZoneEndValue = 0.0f
    
    init {
        val a = context.obtainStyledAttributes(attrs, R.styleable.GaugeView, defStyle, 0)
        scaleStartValue = a.getFloat(R.styleable.GaugeView_scaleStartValue, 0.0f)
        scaleEndValue = a.getFloat(R.styleable.GaugeView_scaleEndValue, 0.0f)
        scaleDivisions = a.getInteger(R.styleable.GaugeView_divisions, 10)
        scaleSubdivisions = a.getInteger(R.styleable.GaugeView_subdivisions, 5)
        hasSecondaryTextSize = a.getBoolean(R.styleable.GaugeView_hasSecondaryTextSize, false)
        scaleColor = a.getColor(R.styleable.GaugeView_scaleColor, Color.BLACK)
        isWarningZoneEnabled = a.getBoolean(R.styleable.GaugeView_isWarningZoneEnabled, false)
        warningZoneStartValue = a.getFloat(R.styleable.GaugeView_warningZoneStartValue, 0.0f)
        warningZoneEndValue = a.getFloat(R.styleable.GaugeView_warningZoneEndValue, 0.0f)
        warningZoneColor = a.getColor(R.styleable.GaugeView_warningZoneColor, Color.RED)
        isMainTextVisible = a.getBoolean(R.styleable.GaugeView_isMainTextVisible, true)
        mainTextUnit = a.getString(R.styleable.GaugeView_mainTextUnit) ?: context.getString(R.string.speed_unit)
        a.recycle()
        
        initScale()
    }
    
    override fun onSizeChanged(w: Int, h: Int, oldw: Int, oldh: Int) {
        drawGauge()
    }
    
    override fun onDraw(canvas: Canvas) {
        drawBackground(canvas)
        val scale = width.coerceAtMost(height).toFloat()
        canvas.scale(scale, scale)
        canvas.translate(
            if (scale == height.toFloat()) (width - scale) / 2 / scale else 0f,
            if (scale == width.toFloat()) (height - scale) / 2 / scale else 0f
        )
        drawArrow(canvas)
        if (isMainTextVisible) drawTextBelowArrow(canvas)
        
        computeCurrentValue()
    }
    
    fun setValue(value: Float) {
        targetValue = when {
            value < scaleStartValue -> {
                scaleStartValue
            }
            value > scaleEndValue -> {
                scaleEndValue
            }
            else -> {
                value
            }
        }
        arrowInitialized = true
        invalidate()
    }
    
    private fun initScale() {
        scaleRotation = (SCALE_START_ANGLE + 180) % 360
        divisionValue = (scaleEndValue - scaleStartValue) / scaleDivisions
        
        subdivisionValue = divisionValue / scaleSubdivisions
        subdivisionAngle = (360 - 2 * SCALE_START_ANGLE) / (scaleDivisions * scaleSubdivisions)
    }
    
    private fun drawGauge() {
        background?.recycle()
        
        background = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888)
        val canvas = Canvas(requireNotNull(background))
        val scale = width.coerceAtMost(height).toFloat()
        canvas.scale(scale, scale)
        canvas.translate(
            if (scale == height.toFloat()) (width - scale) / 2 / scale else 0f,
            if (scale == width.toFloat()) (height - scale) / 2 / scale else 0f
        )
        
        drawGaugeRing(canvas)
        drawScale(canvas)
    }
    
    private fun drawBackground(canvas: Canvas) {
        background?.let {
            canvas.drawBitmap(it, 0f, 0f, backgroundPaint)
        }
    }
    
    private fun drawGaugeRing(canvas: Canvas) {
        canvas.drawCircle(0.5f, 0.5f, hypot(0.48f, scaleRect.top), gaugeRingPaint)
    }
    
    private fun drawScale(canvas: Canvas) {
        canvas.save()
        
        canvas.rotate(scaleRotation, 0.5f, 0.5f)
        
        val totalTicks = scaleDivisions * scaleSubdivisions + 1
        var currentMainTick: Float
        for (i in 0 until totalTicks) {
            val y1 = scaleRect.top - 0.055f
            
            val y2 = y1 + 0.045f
            val y3 = y1 + 0.090f
            
            val value = getValueForTick(i)
            val isWarningZone = isWarningZoneEnabled && value in warningZoneStartValue..warningZoneEndValue
            val paint = if (hasSecondaryTextSize && i % 2 != 0) rangePaintSecondary else rangePaint
            val div = scaleEndValue / scaleDivisions.toFloat()
            val mod = value % div
            paint.strokeWidth = 0.004f
            paint.color = if (!isWarningZone) scaleColor else warningZoneColor
            if (abs(mod - 0) < 0.001 || abs(mod - div) < 0.001) {
                
                currentMainTick = (i.toFloat() / scaleSubdivisions) * ((scaleEndValue - scaleStartValue) / scaleDivisions)
                canvas.drawLine(0.5f, y1, 0.5f, y3 - 0.04f, paint)
                
                paint.style = Style.FILL
                drawText(canvas, valueString(currentMainTick), y3 + 0.01f, paint)
            } else {
                canvas.drawLine(0.5f, y1, 0.5f, y2 - 0.02f, paint)
            }
            canvas.rotate(subdivisionAngle, 0.5f, 0.5f)
        }
        canvas.restore()
    }
    
    private fun drawText(canvas: Canvas, value: String, y: Float, paint: Paint) {
        
        val originalTextSize = paint.textSize
        
        val magnifier = 100f
        
        canvas.save()
        canvas.scale(1f / magnifier, 1f / magnifier)
        
        paint.textSize = originalTextSize * magnifier
        canvas.drawText(value, 0.5f * magnifier, y * magnifier, paint)
        
        canvas.restore()
        paint.textSize = originalTextSize
    }
    
    private fun valueString(value: Float): String {
        return String.format("%d", value.toInt())
    }
    
    private fun getValueForTick(tick: Int): Float {
        return tick * (divisionValue / scaleSubdivisions)
    }
    
    private fun drawArrow(canvas: Canvas) {
        if (arrowInitialized) {
            val angle = getAngleForValue(currentValue)
            with(canvas) {
                save()
                rotate(angle, 0.5f, 0.5f)
                drawPath(arrowLeftPath, arrowPaint)
                drawPath(arrowRightPath, arrowPaint)
                restore()
                drawCircle(0.5f, 0.5f, 0.04f, arrowCirclePaint)
                drawCircle(0.5f, 0.5f, 0.025f, arrowInnerCirclePaint)
            }
        }
    }
    
    private fun drawTextBelowArrow(canvas: Canvas) {
        canvas.drawText("${valueString(targetValue)} $mainTextUnit", 0.5f, 0.78f, textPaint)
    }
    
    private fun getAngleForValue(value: Float): Float {
        return (scaleRotation + value / subdivisionValue * subdivisionAngle) % 360
    }
    
    private fun computeCurrentValue() {
        if (abs(currentValue - targetValue) <= 0.01f) {
            return
        }
        if (-1L != arrowLastMoved) {
            val time = (System.currentTimeMillis() - arrowLastMoved) / 1000.0f
            val direction = sign(arrowVelocity)
            arrowAcceleration = if (abs(arrowVelocity) < 90.0f) {
                5.0f * (targetValue - currentValue)
            } else {
                0.0f
            }
            arrowAcceleration = 5.0f * (targetValue - currentValue)
            currentValue += arrowVelocity * time
            arrowVelocity += arrowAcceleration * time
            if ((targetValue - currentValue) * direction < 0.01f * direction) {
                currentValue = targetValue
                arrowVelocity = 0.0f
                arrowAcceleration = 0.0f
                arrowLastMoved = -1L
            } else {
                arrowLastMoved = System.currentTimeMillis()
            }
            invalidate()
        } else {
            arrowLastMoved = System.currentTimeMillis()
            computeCurrentValue()
        }
    }
    
    companion object {
        private const val ARROW_WIDTH = 0.025f
        private const val ARROW_HEIGHT = 0.35f
        private const val SCALE_POSITION = 0.075f
        private const val SCALE_START_ANGLE = 60.0f
    }
}