package com.ilslv.arrivalspeedometer

import android.app.Service
import android.content.Intent
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch

class SpeedGaugeService: Service() {
    
    private val scope = CoroutineScope(Dispatchers.Default)
    private var job: Job? = null
    private val dataGenerator = DataGenerator()
    
    override fun onBind(p0: Intent?) = object : ISpeedDataGeneratorInterface.Stub() {
        
        override fun generateValues(onDataChangedListener: DataChangedListener?) {
            job = scope.launch {
                dataGenerator.generate(200) {
                    onDataChangedListener?.onDataChanged(it)
                }
            }
        }
    }
    
    override fun onUnbind(intent: Intent?): Boolean {
        job?.cancel()
        return super.onUnbind(intent)
    }
}