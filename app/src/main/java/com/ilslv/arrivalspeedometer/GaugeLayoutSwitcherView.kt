package com.ilslv.arrivalspeedometer

import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.view.animation.AnimationUtils
import android.widget.FrameLayout

class GaugeLayoutSwitcherView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyle: Int = 0
) : FrameLayout(context, attrs, defStyle) {
    
    interface OnGaugeLayoutSwitchedListener {
        
        fun onSwitched(gaugeType: GaugeType)
    }
    
    private val speedGaugeView: GaugeView
    private val velocityGaugeView: GaugeView
    private val rightOut = AnimationUtils.loadAnimation(context, R.anim.slide_right_out)
    private val rightIn = AnimationUtils.loadAnimation(context, R.anim.slide_right_in)
    private val leftIn = AnimationUtils.loadAnimation(context, R.anim.slide_left_in)
    private val leftOut = AnimationUtils.loadAnimation(context, R.anim.slide_left_out)
    
    private var onGaugeLayoutSwitchedListener: OnGaugeLayoutSwitchedListener? = null
    
    init {
        val view = View.inflate(context, R.layout.view_gauge_layout_switcher, this)
        speedGaugeView = view.findViewById(R.id.speedGaugeView)
        velocityGaugeView = view.findViewById(R.id.velocityGaugeView)
        setOnTouchListener(object : SwipeGestureDetector() {

            override fun onSwipeRight() {
                if (velocityGaugeView.visibility == VISIBLE) return
                speedGaugeView.startAnimation(rightOut)
                velocityGaugeView.startAnimation(rightIn)
                speedGaugeView.visibility = View.GONE
                velocityGaugeView.visibility = View.VISIBLE
                onGaugeLayoutSwitchedListener?.onSwitched(GaugeType.VELOCITY)
            }

            override fun onSwipeLeft() {
                if (speedGaugeView.visibility == VISIBLE) return
                velocityGaugeView.startAnimation(leftOut)
                speedGaugeView.startAnimation(leftIn)
                velocityGaugeView.visibility = View.GONE
                speedGaugeView.visibility = View.VISIBLE
                onGaugeLayoutSwitchedListener?.onSwitched(GaugeType.SPEED)
            }
        })
    }
    
    fun setSpeedValue(value: Float) {
        speedGaugeView.setValue(value)
    }
    
    fun setVelocityValue(value: Float) {
        velocityGaugeView.setValue(value)
    }
    
    fun setOnGaugeLayoutSwitchedListener(onGaugeLayoutSwitchedListener: OnGaugeLayoutSwitchedListener) {
        this.onGaugeLayoutSwitchedListener = onGaugeLayoutSwitchedListener
    }
}