package com.ilslv.arrivalspeedometer

import android.view.MotionEvent
import android.view.View
import android.view.View.OnTouchListener
import kotlin.math.abs

abstract class SwipeGestureDetector : OnTouchListener {
    
    private var isSwiping = false
    private var firstFingerStartX = 0f
    private var firstFingerEndX = 0f
    private var secondFingerStartX = 0f
    private var secondFingerEndX = 0f
    
    override fun onTouch(v: View, event: MotionEvent): Boolean {
        if (event.pointerCount > 1) {
            when (event.action and MotionEvent.ACTION_MASK) {
                MotionEvent.ACTION_POINTER_DOWN -> {
                    isSwiping = true
                    firstFingerStartX = event.getX(0)
                    secondFingerStartX = event.getX(1)
                }
                MotionEvent.ACTION_POINTER_UP -> {
                    isSwiping = false
                    val p1Diff: Float = firstFingerStartX - firstFingerEndX
                    val p2Diff: Float = secondFingerStartX - secondFingerEndX
                
                    if (abs(p1Diff) > SWIPE_THRESHOLD && abs(p2Diff) > SWIPE_THRESHOLD &&
                        (p1Diff > 0 && p2Diff > 0 || p1Diff < 0 && p2Diff < 0)
                    ) {
                        if (firstFingerStartX > firstFingerEndX) {
                            onSwipeLeft()
                        } else {
                            onSwipeRight()
                        }
                    }
                }
                MotionEvent.ACTION_MOVE -> if (isSwiping) {
                    firstFingerEndX = event.getX(0)
                    secondFingerEndX = event.getX(1)
                }
            }
        }
        return true
    }
    
    abstract fun onSwipeRight()
    abstract fun onSwipeLeft()
    
    companion object {
        private const val SWIPE_THRESHOLD = 100
    }
}