package com.ilslv.arrivalspeedometer

enum class GaugeType {
    SPEED, VELOCITY
}