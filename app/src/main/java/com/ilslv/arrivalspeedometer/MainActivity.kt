package com.ilslv.arrivalspeedometer

import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.ServiceConnection
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.IBinder
import android.view.WindowInsets
import android.view.WindowInsetsController
import android.view.WindowManager

class MainActivity : AppCompatActivity() {
    
    private val layoutSwitcherView by lazy { findViewById<GaugeLayoutSwitcherView>(R.id.container) }
    private var dataGenerator: ISpeedDataGeneratorInterface? = null
    private var visibleGaugeType = GaugeType.SPEED
    
    private val serviceConnection = object : ServiceConnection {
        override fun onServiceConnected(name: ComponentName?, service: IBinder?) {
            dataGenerator = ISpeedDataGeneratorInterface.Stub.asInterface(service)
            dataGenerator?.generateValues(object : DataChangedListener.Stub() {
                override fun onDataChanged(value: Float) {
                    layoutSwitcherView.post {
                        if (visibleGaugeType == GaugeType.SPEED) {
                            layoutSwitcherView.setSpeedValue(value)
                        } else {
                            layoutSwitcherView.setVelocityValue(value)
                        }
                    }
                }
            })
        }
        
        override fun onServiceDisconnected(name: ComponentName?) {
            dataGenerator = null
        }
    }
    
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setFullscreen()
        layoutSwitcherView.setOnGaugeLayoutSwitchedListener(object :
            GaugeLayoutSwitcherView.OnGaugeLayoutSwitchedListener {
            
            override fun onSwitched(gaugeType: GaugeType) {
                this@MainActivity.visibleGaugeType = gaugeType
                unbindService(serviceConnection)
                bind(
                    if (gaugeType == GaugeType.SPEED) {
                        SpeedGaugeService::class.java
                    } else {
                        VelocityGaugeService::class.java
                    }
                )
            }
        })
    }
    
    override fun onStart() {
        super.onStart()
        bind(SpeedGaugeService::class.java)
    }
    
    override fun onStop() {
        super.onStop()
        unbindService(serviceConnection)
    }
    
    private fun setFullscreen() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
            window.attributes.layoutInDisplayCutoutMode =
                WindowManager.LayoutParams.LAYOUT_IN_DISPLAY_CUTOUT_MODE_SHORT_EDGES
        }
        
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
            window.setDecorFitsSystemWindows(false)
            window.insetsController?.apply {
                hide(WindowInsets.Type.statusBars() or WindowInsets.Type.navigationBars())
                systemBarsBehavior = WindowInsetsController.BEHAVIOR_SHOW_TRANSIENT_BARS_BY_SWIPE
            }
        }
    }
    
    private fun bind(serviceName: Class<*>) {
        bindService(
            Intent(this, serviceName),
            serviceConnection,
            Context.BIND_AUTO_CREATE
        )
    }
}