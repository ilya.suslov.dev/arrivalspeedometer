package com.ilslv.arrivalspeedometer

import kotlinx.coroutines.delay

class DataGenerator {
    
    private enum class GenerationDirection {
        UP, DOWN
    }
    
    private var generationDirection = GenerationDirection.UP
    
    suspend fun generate(maxValue: Int, onChanged: (Float) -> Unit) {
        repeat(Int.MAX_VALUE) {
            if (generationDirection == GenerationDirection.UP) {
                for (i in 0..maxValue) {
                    if (i == maxValue) generationDirection = GenerationDirection.DOWN
                    onChanged.invoke(i.toFloat())
                    delay(50)
                }
            } else {
                for (i in maxValue downTo 0) {
                    if (i == 0) generationDirection = GenerationDirection.UP
                    onChanged.invoke(i.toFloat())
                    delay(50)
                }
            }
        }
    }
}